= Procesiranje

== Direktive

If you don’t want a conditional preprocessor directive to be processed, you must escape it using a backslash.

  \ifdef::just-an-example[]

=== ifdef Directive

.ifdef example
----
ifdef::env-github[]
This content is for GitHub only.
endif::[]
----

.ifdef long-form example
----
ifdef::env-github[]
This content is for GitHub only.

So much content in this section, I'd get confused reading the source without the closing `ifdef` directive.

It isn't necessary for short blocks, but if you are conditionally including a section it may be something worth considering.

Other readers reviewing your docs source code may go cross-eyed when reading your source docs if you don't.
endif::env-github[]
----

.ifdef single line example
----
ifdef::revnumber[This document has a version number of {revnumber}.]
----

ifdef::revnumber[This document has a version number of {revnumber}.]

The single-line block above is equivalent to this formal ifdef directive:
----
ifdef::revnumber[]
This document has a version number of {revnumber}.
endif::[]
----

=== ifndef Directive

ifndef is the logical opposite of ifdef. Content between ifndef and endif gets included only if the specified attribute is not set:

.ifndef Example
----
ifndef::env-github[]
This content is not shown on GitHub.
endif::[]
----

'''

== Tabela

.Pipe in table: pass:[{vbar}]
[cols="3"]
|===
^|Prefix the `{vbar}` with `{caret}` to center content horizontally
<|Prefix the `{vbar}` with `<` to align the content to the left horizontally
>|Prefix the `{vbar}` with `>` to align the content to the right horizontally

.^|Prefix the `{vbar}` with a `.` and `{caret}` to center the content in the cell vertically
.<|Prefix the `{vbar}` with a `.` and `<` to align the content to the top of the cell
.>|Prefix the `{vbar}` with a `.` and `>` to align the content to the bottom of the cell

3+^.^|This content spans three columns (`3{plus}`) and is centered horizontally (`{caret}`) and vertically (`.{caret}`) within the cell.
|===

'''

== List

.Ordered list numbering scheme by level
|===
|Level|Numbering Scheme|Examples|CSS class (HTML converter)

|1
|Arabic
|1. 2. 3.
|arabic

|2
|Lower Alpha
|a. b. c.
|loweralpha

|3
|Lower Roman
|i. ii. iii.
|lowerroman

|4
|Upper Alpha
|A. B. C.
|upperalpha

|5
|Upper Roman
|I. II. III.
|upperroman
|===

You can override the number scheme for any level by setting its style (the first positional entry in a block attribute list). You can also set the starting number using the start attribute:

----
[lowerroman, start=5]
. Five
. Six
[loweralpha]
.. a
.. b
.. c
. Seven
----

.Result
You can override the number scheme for any level by setting its style (the first positional entry in a block attribute list). You can also set the starting number using the start attribute:

[lowerroman, start=5]
. Five
. Six
[loweralpha]
.. a
.. b
.. c
. Seven

'''

== Built-in blocks summary

The following table identifies the built-in block names and delimited blocks syntax, their purposes, and the substitutions performed on their contents.

.Tabela
|===
|Block|Name|Delimiter|Purpose|Substitutions

|Admonition
|[<LABEL>]
|Any delimiter
|Content labeled with a tag or icon, can masquerade as any delimited block type
|Varies

|Comment
|N/A
|pass:[////]
|Private notes that are not displayed in the output
|None

|Example
|[example]
|pass:[====]
|Designates example content or defines an admonition block
|Normal

|Fenced
|N/A
|pass:[```]
|Source code or keyboard input is displayed as entered
|Verbatim

|Listing
|[listing]
|pass:[----]
|Source code or keyboard input is displayed as entered
|Verbatim

|Literal
|[literal]
|pass:[....]
|Output text is displayed exactly as entered
|Verbatim

|Open
|Most block names
|pass:[--]
|Anonymous block that can act as any block except `passthrough` or `table` blocks
|Varies

|Passthrough
|[pass]
|pass:[++++]
|Unprocessed content that is sent directly to the output
|None

|Quote
|[quote]
|pass:[____]
|A quotation with optional attribution
|Normal

|Sidebar
|[sidebar]
|pass:[****]
|Aside text and content displayed outside the flow of the document
|Normal

|Source
|[source]
|pass:[----]
|Source code or keyboard input to be displayed as entered
|Verbatim

|Stem
|[stem]
|pass:[++++]
|Unprocessed content that is sent directly to an interpreter (such as AsciiMath or LaTeX math)
|None

|Table
|N/A
|{vbar}===
|Displays tabular content
|Varies

|Verse
|[verse]
|pass:[____]
|A verse with optional attribution
|Normal
|===

'''

== Quote

.Anatomy of a basic quote
----
[quote, attribution, citation title and information]
Quote or excerpt text
----

.After landing the cloaked Klingon bird of prey in Golden Gate park: 
[quote, Captain James T. Kirk, Star Trek IV: The Voyage Home]     
Everybody remember where we parked. 


.Quote block syntax
[quote, Monty Python and the Holy Grail]
____
Dennis: Come and see the violence inherent in the system. Help! Help! I'm being repressed!

King Arthur: Bloody peasant!

Dennis: Oh, what a giveaway! Did you hear that? Did you hear that, eh? That's what I'm on about! Did you see him repressing me? You saw him, Didn't you?
____

=== Quoted paragraph

.Quoted paragraph syntax
"I hold it that a little rebellion now and then is a good thing,
and as necessary in the political world as storms in the physical."
-- Thomas Jefferson, Papers of Thomas Jefferson: Volume 11

=== Air quotes

[, James Baldwin]
""
Not everything that is faced can be changed.
But nothing can be changed until it is faced.
""

=== Verse

.Verse paragraph syntax
[verse, Carl Sandburg, two lines from the poem Fog]
The fog comes
on little cat feet.

.Verse delimited block syntax
[verse, Carl Sandburg, Fog]
____
The fog comes
on little cat feet.

It sits looking
over harbor and city
on silent haunches
and then moves on.
____

'''

== Literal Text and Blocks

.Literal style paragraph syntax
----
[literal]
error: The requested operation returned error: 1954 Forbidden search for defensive operations manual
absolutely fatal: operation initiation lost in the dodecahedron of doom
would you like to die again? y/n
----

.Result
[literal]
error: The requested operation returned error: 1954 Forbidden search for defensive operations manual
absolutely fatal: operation initiation lost in the dodecahedron of doom
would you like to die again? y/n

.Literal delimited block syntax
----
....
Lazarus: Where is the *defensive operations manual*?

Computer: Calculating ...
Can not locate object that you are not authorized to know exists.
Would you like to ask another question?

Lazarus: Did the werewolves tell you to say that?

Computer: Calculating ...
....
----

.Result
....
Lazarus: Where is the *defensive operations manual*?

Computer: Calculating ...
Can not locate object that you are not authorized to know exists.
Would you like to ask another question?

Lazarus: Did the werewolves tell you to say that?

Computer: Calculating ...
....

=== Listing Blocks

.Listing paragraph syntax
----
[listing]
This is an example of a paragraph styled with `listing`.
Notice that the monospace markup is preserved in the output.
----

.Result
[listing]
This is an example of a paragraph styled with `listing`.
Notice that the monospace markup is preserved in the output.


.Source block syntax
------
.app.rb
[source,ruby]
----
require 'sinatra'

get '/hi' do
  "Hello World!"
end
----
------

Result

.app.rb
[source,ruby]
----
require 'sinatra'

get '/hi' do
  "Hello World!"
end
----

=== Passthrough Macros

inline pass macro::
An inline macro named pass that can be used to passthrough content. Supports an optional set of substitutions.

  pass:[content like #{variable} passed directly to the output] followed by normal content.

  content with only select substitutions applied: pass:c,a[__<{email}>__]

single and double plus::
A special syntax for preventing text from being formatted. Only escapes special characters for compliance with the output format and doesn’t support explicit substitutions.

triple plus::
A special syntax for designating passthrough content. Does not apply any substitutions (equivalent to the inline pass macro) and doesn’t support explicit substitutions.

----
pass:[content like #{variable} passed directly to the output] followed by normal content.

content with only select substitutions applied: pass:c,a[__<{email}>__]
----

pass:[content like #{variable} passed directly to the output] followed by normal content.

content with only select substitutions applied: pass:c,a[__<{email}>__]


==== Inline pass macro and explicit substitutions

If you want to enable ad-hoc quotes substitution, then assign the macros value to subs and use the inline pass macro.

------
[subs=+macros] <1>
----
I better not contain *bold* or _italic_ text.
pass:quotes[But I should contain *bold* text.] <2>
----

<1> macros is assigned to subs, which allows any macros within the block to be processed.
<2> The pass macro is assigned the quotes value. Text within the square brackets will be formatted.
------

[subs=+macros]
----
I better not contain *bold* or _italic_ text.
pass:quotes[But I should contain *bold* text.]
----


The inline pass macro also accepts shorthand values for specifying substitutions.

* c = special characters
* q = quotes
* a = attributes
* r = replacements
* m = macros
* p = post replacements

For example, the quotes text substitution value is assigned in the inline passthrough macro below:


==== Triple plus passthrough

The triple-plus passthrough works much the same way as the pass macro. To exclude content from substitutions, enclose it in triple pluses (pass:[+++]).

  +++content passed directly to the output+++ followed by normal content.


==== Nesting blocks and passthrough macros

------
[source,java,subs="+quotes,+macros"]
----
protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
            **.antMatchers("/resources/$$**$$").permitAll()**
            .anyRequest().authenticated()
            .and()
        .formLogin()
            .loginPage("/login")
            .permitAll();
----
------

[source,java,subs="+quotes,+macros"]
----
protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
            **.antMatchers("/resources/$$**$$").permitAll()**
            .anyRequest().authenticated()
            .and()
        .formLogin()
            .loginPage("/login")
            .permitAll();
----

=== Passthrough Blocks

The passthrough block and pass block style exclude block content from all substitutions (unless specified otherwise).

A passthrough block is delimited by four plus signs (pass:[++++]). The pass style is implied.

----
++++
<video poster="images/movie-reel.png">
  <source src="videos/writing-zen.webm" type="video/webm">
</video>
++++
----

'''

== Open Blocks

The most versatile block of all is the open block.

.Open block syntax
----
--
An open block can be an anonymous container,
or it can masquerade as any other block.
--
----

.Result: Open block
--
An open block can be an anonymous container,
or it can masquerade as any other block.
--


.Open block masquerading as a sidebar syntax
----
[sidebar]
.Related information
--
This is aside text.

It is used to present information related to the main content.
--
----

[sidebar]
.Related information
--
This is aside text.

It is used to present information related to the main content.
--

'''

.Customizing a source block with Pygments attributes

------
:source-highlighter: pygments
:pygments-style: manni
:pygments-linenums-mode: inline

[source,ruby,linenums]
----
ORDERED_LIST_KEYWORDS = {
  'loweralpha' => 'a',
  'lowerroman' => 'i',
  'upperalpha' => 'A',
  'upperroman' => 'I'
   #'lowergreek' => 'a'
   #'arabic'     => '1'
   #'decimal'    => '1'
}
----
------

[source,ruby,linenums]
----
ORDERED_LIST_KEYWORDS = {
  'loweralpha' => 'a',
  'lowerroman' => 'i',
  'upperalpha' => 'A',
  'upperroman' => 'I'
   #'lowergreek' => 'a'
   #'arabic'     => '1'
   #'decimal'    => '1'
}
----


.Highlight select lines in a source block
------
:source-highlighter: pygments

[source,ruby,highlight=2..5]
----
ORDERED_LIST_KEYWORDS = {
  'loweralpha' => 'a',
  'lowerroman' => 'i',
  'upperalpha' => 'A',
  'upperroman' => 'I',
}
----
------

[source,ruby,highlight=2..5]
----
ORDERED_LIST_KEYWORDS = {
  'loweralpha' => 'a',
  'lowerroman' => 'i',
  'upperalpha' => 'A',
  'upperroman' => 'I',
}
----


[source,ruby]
----
require 'sinatra' <1>

get '/hi' do <2> <3>
  "Hello World!"
end
----
<1> Library import
<2> URL mapping
<3> Response block


