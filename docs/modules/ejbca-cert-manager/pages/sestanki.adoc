
= Sestanki

[sidebar]
.Seznam sestankov
****
Seznam sestankov, prisotnosti, teme, ugotovitve
****

{counter:sestanek}. Sestanek : 27.02.2019

[cols="^.^1h,8"]
|===

|Tema 
|Uvodni sestanek, da se spoznamo med sabo in z temo. Predstavljeno je konceptualno delovanje aplikacije. Poudarjen je prenos odgovornosti skrbništva za certifikate na skrbnike.

|Vabljeni 
|
Primož Rozman, Urška Rapaić, Živkovič Miha, Radović Veljko, Horvat Boštjan, Dušan Fefer

|Prisotni
|
Primož Rozman, Živkovič Miha, Radović Veljko, Horvat Boštjan, Dušan Fefer

|===

Zaključek::
* [x] Preverti moramo kvaliteto podatkov
* Povprašati skrbnike:
** [x] kakšen je njihov proces danes 
** [x] kako si predstavljajo svoje delo v bodoče.
* [x] Napisati je potrebno specifikacijo za aplikacijo. (delno je že)
* [x] Koliko ČD je potrebno za izvedbo

'''

{counter:sestanek}. Sestanek : 14.03.2019

[cols="^.^1h,8"]
|===

|Tema 
|Preverjanje kvalitete obstoječih podatkov. Želje uporabnikov pri delu z aplikacijo. Katere podatke lahko pridobimo iz drugih virov? Odgovornost za certifikate je na skrbnikih.

|Vabljeni 
|
Novak Primož, 
Bidovec Andrej, 
Živkovič Miha, 
Radović Veljko, 
Horvat Boštjan, 
Dušan Fefer

|Prisotni
|
Novak Primož, 
Horvat Boštjan, 
Dušan Fefer, 

|===

Zaključek::
* [x] Obstoječi podatki so nepopolni in nekvalitetni.
* [x] Uporabno je polje `CN` in šifra `SPP` iz Mobile Mercury (MM)
* [x] Predlagana je sinhronizacija iz MM (dnevna in na zahtevo)
* [x] Dotaknili smo se na kratko trenutnega procesa.

Zaradi odsotnosti Mihe in Veljka, ne moremo odgovoriti na nekatera vprašanja skrbnikov.

'''

{counter:sestanek}. Sestanek : 20.03.2019

[cols="^.^1h,8"]
|===

|Tema 
|Seznanitev s  kvalitet obstoječih podatkov. Uporabniki/skrbniki: pričakovanje pri delu z aplikacijo. Katere podatke lahko pridobimo iz drugih virov?

|Vabljeni 
|
Novak Primož, 
Bidovec Andrej, 
Živkovič Miha, 
Radović Veljko, 
Horvat Boštjan, 
Dušan Fefer, 

|Prisotni
|
Bidovec Andrej, 
Živkovič Miha, 
Radović Veljko, 
Horvat Boštjan, 
Dušan Fefer

|===

Zaključek::
* [x] Obstoječi podatki so nepopolni in nekvalitetni.
* [x] Uporabno je polje `CN` in šifra iz Mobile Mercury `SPP`
* [x] Sinhronizacija iz MM (dnevna in na zahtevo), *je možna*
* [x] Dogovorili smo se na glede procesa izdaje.

Kaj moramo še narediti/doreči (Dušan Fefer)::
* [x] pregled certifikatov (iskanje/filter)
* [ ] vnos nove stranke (vnos SPP in refresh iz MM)
* [x] proces izdaje in obnove certifikata
* [x] Potrebujemo specifikacijo za virtualko
* [x] Potrebujemo specifikacijo za bazo
* [ ] katere port moramo odpreti in kam (MS SQL baza, EJBCA)
* [ ] izdaja certov za aplikacijo (certifikat za samo izdajo, certifikat za web strežnik)
* [ ] Administrativni del (povezovanje)

'''

{counter:sestanek}. Sestanek : 25.03.2019

[cols="^.^1h,8"]
|===

|Tema 
a|
* načrtovanje baze
* uporabniški del
* administrativni del

|Vabljeni 
a|
. Živkovič Miha
. Radović Veljko
. Dušan Fefer

|Prisotni
a|
. Radović Veljko
. Dušan Fefer
|===

Zaključek::
* [x] Stranka ima lahko več kontaktov: *nova tabela za kontakte*
* [x] Dogovor med Veljkom in Miho, kaj bo kdo delal. (dogovorjeno : *Veljko BackEnd*, *Miha FrontEnd*)
* [ ] Git: dokumentacija in koda je v Git-u (Slapar to ureja)
* [ ] Git: uporabnike : e-radovicv, zivkovicm in feferd poslal Slapar-ju, ki to ureja.
* [x] Spremeba v bazi: *DELETE* ne obstaja. (Veljko in Dušan)
* [x] Spremeba v bazi: uporabljajo se *valid_from in valid_to*, tam kjer je smiselno.
* [x] Potrebujemo uporabnika *TS AD sistemski uprabnik* za aplikacijo in dostop do baze (28.03. oddal Jiro: dobil 28.03.)
* [x] Potrebujemo SQL Testno Bazo (28.03. oddal Jiro: dobil 29.03.)
* [x] Potrebujemo Testni Virtualni strežnik (28.03. oddal Jiro: dobil ??? )
* [ ] Definirati je potrebno, katera polja za stranko želimo iz MM. (Tomaž in Andrej)
* [ ] Definirati MM "Report" za stranko iz MM. (kontakt Gros)
* [x] Diagram poteka izdaje certifikata s procesi. (Dušan)
* [ ] Napisati navodila za stranke, ker je ob prehodu na EJBCA certifikat potrebno nastaviti tudi nov ROOT certifikat, drugače ne bo delalo. (Boštjan)

Moj predlog::
. na začetku vnesemo *distinct sppid* iz excela v *tabelo strank*.
. Sledi sinhronizacija z MM (ime, kontakt/kateri? in telefon/kateri?)
. v drugem koraku vnesemo *CN in SSPID* in vnesemo v *tabelo certifikatov*, da poberemo *CN in datume veljavnosti certifikata*. To bomo potrebovali, da vemo kdaj obstoječ certifikat poteča in da je potrebna izdaja novega.

