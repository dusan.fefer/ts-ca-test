[colophon]
= {doctitle}
:made_by: Ustvaril 
:made_with: Narejeno z 
:designed_by: Oblikovanje 
:version_date: {version-label} z dne 
:revision_remarks: Komentar revizije  
:copyright1: Zaščiteno proti kopiranju
:copyright2: Vse pravice so pridržane

{made_by}::
{author}

// {copyright1} (C) 2019 {author}. {copyright2}.

(C) 2019 Telekom Slovenije

// Izdal : {author}

{designed_by}::
{author}

{made_with}::
Asciidoctor Docker, Ubuntu, Visual Studio Code.

'''

{version-label}::
{revnumber}

{version_date}::
{revdate}

ifeval::["{revremark}" != ""]
{revision_remarks}::
{revremark}
endif::[]
